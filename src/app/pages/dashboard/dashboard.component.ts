import { Component, OnInit } from '@angular/core';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  templateUrl: './dashboard.pug',
  styleUrls: ['./dashboard.scss']
})

export class DashboardComponent implements OnInit {

  constructor(private modalService: NgbModal) {}

  timeInEST: string;
  localTime: string;

  ngOnInit() {
    this.getTime();
  }

  openModal(content) {
    this.modalService.open(content, {
      backdropClass: 'no-backdrop',
      windowClass: 'suite-modal',
      centered: true,
    });
  }

  checkTime(i) {
      if (i < 10) {
          i = "0" + i;
      }
      return i;
  }

  getTime() {
    var today = new Date();
    this.timeInEST = today.toLocaleString('us-US', {hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true, timeZone: 'America/New_York' })
    this.localTime = today.toLocaleString('us-US', {hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true, timeZone: "Asia/Shanghai" })
    // var today = new Date();
    // var h = today.getHours();
    // var ampm = h >= 12 ? 'pm' : 'am';
    // h = h % 12;
    // h = h ? h : 12;
    // var m = today.getMinutes();
    // var s = today.getSeconds();
    // h = this.checkTime(h);
    // m = this.checkTime(m);
    // s = this.checkTime(s);
    // this.timeInEST = h+":"+m+":"+s+" "+ampm;

    var func = this;
    var t = setTimeout(function () {
      func.getTime();
    }, 1000);
  }
}
